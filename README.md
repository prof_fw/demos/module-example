## Overview 

This is a demo workflow based on the python boilerplate repository for Flyte, [Repo](https://github.com/flyteorg/flytekit-python-template)

The basis for this document is from the [getting started with Flyte guide](https://docs.flyte.org/en/latest/getting_started_scale.html#gettingstarted-scale)

## Usage

```bash
pip install -r requirements.txt
flytectl sandbox start --source .  
```

**Add Env Vars to shell**

```bash
pip install -r requirements.txt
flytectl sandbox start --source .  
flytectl config init  
flytectl sandbox exec -- docker build . --tag "myapp:v1"   
pyflyte --pkgs myapp.workflows package --image  "myapp:v1"  
flytectl register files --project flytesnacks --domain development --archive flyte-package.tgz --version v1  
flytectl get workflows --project flytesnacks --domain development myapp.workflows.example.my_wf --version v1 -o doturl
```

## Working with the Sandbox version of Flyte 

### Overview 

The Flyte Sandbox is a fully standalone minimal environment for running Flyte. 
To simplify this further flytectl sandbox provides a simplified way of running flyte-sandbox as a single Docker 
container running locally. The follow section explains how you can use each of these modes and provides more 
information. We recommend running the sandbox using flytectl locally on your workstation or on a single cloud 
instance to try out Flyte or for testing new ideas. Flyte Sandbox is not a complete representation of Flyte, 
many features are intentionally removed from this environment to ensure that the startup times and runtime 
footprints are low.

### Flyte Sandbox as a single Docker container

flytectl sandbox starts a local sandbox environment for Flyte. This is mini-replica of an entire Flyte deployment, 
without the scalability and with minimal extensions. The idea for this environment originated from the desire of 
the core team to make it extremely simple for users of Flyte to try out the platform and get a feel for the user 
experience, without having to understand Kubernetes or dabble with configuration etc. The Flyte single container 
sandbox is also used by the team to run continuous integration tests and used by the flytesnacks - UserGuide 
playground environment. The sandbox can be run in most any environment that supports Docker containers and an
Ubuntu docker base image.

[More information about the Flyte Sandbox can be found here](https://docs.flyte.org/en/latest/deployment/sandbox.html#deployment-sandbox)

## Inspecting Docker container

As the flyte container is a deployed into the Docker in Docker Sandbox container. To see what is happening at the 
container level you need to shell into the sandbox container. 

### Example of booting container with demo workflow
```bash
> docker ps
> docker exec -it <cr.flyte.org/flyteorg/flyte-sandbox:dind image> sh
> docker image ls
> docker run -d -it <myapp:v1 image> /bin/bash
> docker ps
> docker exec -it <myapp:v1 container> sh
> ls
Dockerfile  LICENSE  README.md	boilerplate  docker_build_and_tag.sh  flyte.config  myapp  requirements.txt
> python myapp/workflows/example.py
{"asctime": "2021-09-16 09:05:27,929", "name": "flytekit", "levelname": "DEBUG", "message": "Task returns unnamed native tuple <class 'str'>"}
{"asctime": "2021-09-16 09:05:27,932", "name": "flytekit", "levelname": "DEBUG", "message": "Task returns unnamed native tuple <class 'str'>"}
{"asctime": "2021-09-16 09:05:27,945", "name": "flytekit", "levelname": "INFO", "message": "Invoking __main__.say_hello with inputs: {}"}
INFO:flytekit:Invoking __main__.say_hello with inputs: {}
{"asctime": "2021-09-16 09:05:27,945", "name": "flytekit", "levelname": "INFO", "message": "Task executed successfully in user level, outputs: hello world"}
INFO:flytekit:Task executed successfully in user level, outputs: hello world
Running my_wf() hello world
>
```
Here we can see we have booted into the workflow container, we can see the directory structure, check the env vars and run a workflow.


